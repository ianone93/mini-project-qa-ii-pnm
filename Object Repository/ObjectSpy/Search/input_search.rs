<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_search</name>
   <tag></tag>
   <elementGuidId>165eb40d-f417-4b87-8cfc-e98a73cbe9b5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#small-searchterms</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='small-searchterms']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>c7011da8-8f26-4675-9bbf-0418b76a5648</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>cf05dc8f-84cb-4aee-a479-65a401d21dcd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>search-box-text ui-autocomplete-input</value>
      <webElementGuid>aae6845f-f420-47c9-b722-73d925e746cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>small-searchterms</value>
      <webElementGuid>9db828df-5afb-4df9-8108-fcbb523dec52</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>73debbc3-dc2d-443e-943c-78f9eea5fa96</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Search store</value>
      <webElementGuid>aa192359-85bd-46f0-98dd-e0eacb662362</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>q</value>
      <webElementGuid>f4496bb0-8cd8-44f7-83aa-5a1857bef2a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;small-searchterms&quot;)</value>
      <webElementGuid>678a3d63-a2cc-4ac5-86eb-6062ff34fb6b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='small-searchterms']</value>
      <webElementGuid>c1e6cd82-0ad6-4054-a9a6-6f579a50e01c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>abbbe4e6-0a80-4330-a665-46fb23a1d9c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @id = 'small-searchterms' and @name = 'q']</value>
      <webElementGuid>31e6827b-3416-45a2-81a8-6f7f66aea1e9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
