<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Fromto_Pf</name>
   <tag></tag>
   <elementGuidId>8c94fcbc-c619-4658-bb98-b212bf83930c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#Pf</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='Pf']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>07dd2b9f-e177-4743-b0b9-d67b58a0a44c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>price-from</value>
      <webElementGuid>a5408469-307a-43de-bc6a-4af053dbf161</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Pf</value>
      <webElementGuid>1e98cb5c-8494-40ba-a375-95fcf363b57d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Pf</value>
      <webElementGuid>604fa788-0bec-44fd-94e1-40142596e28a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>863df006-1169-4e10-992f-033655c90aac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Pf&quot;)</value>
      <webElementGuid>5da2681b-8574-46bb-8497-19ff8337412a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='Pf']</value>
      <webElementGuid>9748b0b2-bfbb-4adb-b349-2237eb2cde3b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='advanced-search-block']/div[4]/span/input</value>
      <webElementGuid>4357b224-f440-439b-a924-77f51e357feb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/input</value>
      <webElementGuid>f55b1b5d-9f40-447a-ac07-338f06449739</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'Pf' and @name = 'Pf' and @type = 'text']</value>
      <webElementGuid>86a78569-2ebf-4429-98ff-1c4efd5a3c71</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
