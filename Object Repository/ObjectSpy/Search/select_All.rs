<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_All</name>
   <tag></tag>
   <elementGuidId>ef5a91db-ce18-4be8-b5b4-bc75be59f1ae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#Cid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='Cid']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>08afc1ed-2c8d-40f3-969b-e2c1595b89e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>852a18ae-5c5f-43ae-a2cc-237476aca215</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val-number</name>
      <type>Main</type>
      <value>The field Category must be a number.</value>
      <webElementGuid>0dde77cb-05b1-4612-a46e-23dd65d89674</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Cid</value>
      <webElementGuid>fac01e34-573a-4d0a-84b7-4b686be6b84d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Cid</value>
      <webElementGuid>27937711-d9a3-46b8-a926-418dbac9edad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>All
Books
Computers
Computers >> Desktops
Computers >> Notebooks
Computers >> Accessories
Electronics
Electronics >> Camera, photo
Electronics >> Cell phones
Apparel &amp; Shoes
Digital downloads
Jewelry
Gift Cards
</value>
      <webElementGuid>5902f0cf-9925-421e-93c8-6b6e7db4d4e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Cid&quot;)</value>
      <webElementGuid>10656a21-4b39-4af5-b47e-76afc5225c65</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='Cid']</value>
      <webElementGuid>bee9e778-f262-4a0e-ad24-58c5ea2e4608</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='advanced-search-block']/div/select</value>
      <webElementGuid>20f98488-ccb6-4925-acd9-39e2c947bd3e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Category:'])[1]/following::select[1]</value>
      <webElementGuid>68963a34-8cc2-4c65-9f31-f47098793f0e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Advanced search'])[1]/following::select[1]</value>
      <webElementGuid>47678b5d-5309-4736-b3d3-a86c89922f85</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Automatically search sub categories'])[1]/preceding::select[1]</value>
      <webElementGuid>378b0b86-5041-4e86-b52b-6020a82a7ea9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Manufacturer:'])[1]/preceding::select[1]</value>
      <webElementGuid>2674fda2-5b3a-4195-84e4-9f83ee16e186</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>52215a08-60cb-4bcc-bf84-10e8861be906</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'Cid' and @name = 'Cid' and (text() = 'All
Books
Computers
Computers >> Desktops
Computers >> Notebooks
Computers >> Accessories
Electronics
Electronics >> Camera, photo
Electronics >> Cell phones
Apparel &amp; Shoes
Digital downloads
Jewelry
Gift Cards
' or . = 'All
Books
Computers
Computers >> Desktops
Computers >> Notebooks
Computers >> Accessories
Electronics
Electronics >> Camera, photo
Electronics >> Cell phones
Apparel &amp; Shoes
Digital downloads
Jewelry
Gift Cards
')]</value>
      <webElementGuid>38ff7b05-e9f3-4c4f-8b58-b581c98ed3e6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
