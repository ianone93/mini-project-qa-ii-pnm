<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>results_text_thumbnail</name>
   <tag></tag>
   <elementGuidId>29cfabe1-9e29-4ab9-bd9e-a966f2a33eb1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h2.product-title > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Computing and Internet')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>ec818943-814d-4066-9be2-942865a578fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/computing-and-internet</value>
      <webElementGuid>00368c2e-c267-4250-b964-035d0321eb79</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Computing and Internet</value>
      <webElementGuid>b8d12d3b-7db4-4d3a-b347-4fce0c6a30aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page search-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;search-results&quot;]/div[@class=&quot;product-grid&quot;]/div[@class=&quot;item-box&quot;]/div[@class=&quot;product-item&quot;]/div[@class=&quot;details&quot;]/h2[@class=&quot;product-title&quot;]/a[1]</value>
      <webElementGuid>f85eeb51-0639-41bc-b7f0-d348aaea5adb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Computing and Internet')]</value>
      <webElementGuid>2af4c362-8ddd-41e4-9913-735739c73791</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::a[2]</value>
      <webElementGuid>2052c81c-ee2d-4879-99f7-3180686e9d1e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Display'])[1]/following::a[2]</value>
      <webElementGuid>b0fffaad-5c9b-4247-8f42-e2d2d9b2445c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='More Than 100 tips about computing and internet.'])[1]/preceding::a[1]</value>
      <webElementGuid>108eadf3-b099-4743-ab4c-68a200dfc123</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy of Computing and Internet EX'])[1]/preceding::a[2]</value>
      <webElementGuid>4f6a3fdb-8ffe-4b2b-b74c-71c455193e9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Computing and Internet']/parent::*</value>
      <webElementGuid>4af0544a-0176-47fb-8013-6536f27d7af1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/computing-and-internet')])[2]</value>
      <webElementGuid>37c9d977-2069-4a05-a366-01214855d247</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h2/a</value>
      <webElementGuid>9945d4c4-22c3-4d68-9a98-adb3f8bcd65f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/computing-and-internet' and (text() = 'Computing and Internet' or . = 'Computing and Internet')]</value>
      <webElementGuid>436bdd66-4bba-4044-a7ff-7fb584b0c9ba</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
