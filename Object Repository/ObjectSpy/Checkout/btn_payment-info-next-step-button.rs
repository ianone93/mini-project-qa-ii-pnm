<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_payment-info-next-step-button</name>
   <tag></tag>
   <elementGuidId>affa51a2-e916-444c-84a6-559c765b3ee5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@value='Continue'])[5]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input.button-1.payment-info-next-step-button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>68800aaa-969f-44e8-9733-ed8a0778c5e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>8d546038-95ad-4247-ad7a-6e29e03dd6a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button-1 payment-info-next-step-button</value>
      <webElementGuid>34eefb78-527f-42b7-b084-ca9e78b302f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>PaymentInfo.save()</value>
      <webElementGuid>42f440d8-3b25-4c88-9898-b6ab5315dd6d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Continue</value>
      <webElementGuid>14de9088-fa6b-4b2e-86cf-45059f753e9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;payment-info-buttons-container&quot;)/input[@class=&quot;button-1 payment-info-next-step-button&quot;]</value>
      <webElementGuid>973f37c2-6167-44b6-ada9-cdce52d625bb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@value='Continue'])[5]</value>
      <webElementGuid>bb565c03-094f-458c-ad6e-a53a0e683303</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='payment-info-buttons-container']/input</value>
      <webElementGuid>a181eeed-7b78-42e5-8053-e18ab06710e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[5]/div[2]/div/input</value>
      <webElementGuid>47b4b4d4-af64-4f28-b522-371270091770</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'button']</value>
      <webElementGuid>87a7a762-da2c-4eb3-890d-a1f9e780877c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
