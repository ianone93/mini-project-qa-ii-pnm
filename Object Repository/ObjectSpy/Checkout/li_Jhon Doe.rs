<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Jhon Doe</name>
   <tag></tag>
   <elementGuidId>76d8f329-a802-4b1d-8c01-90ba7d1d3e1c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul[2]/li[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ul.shipping-info > li.name</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>bd331c4c-248e-4242-a94e-b4d937972e11</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>name</value>
      <webElementGuid>1999ec96-2a16-4cd7-92b9-4d404d1f2b44</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Jhon Doe
                    </value>
      <webElementGuid>6b365e46-ff62-4334-881b-992e45fd010f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-confirm-order-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;order-summary-body&quot;]/div[@class=&quot;order-summary-content&quot;]/div[@class=&quot;order-review-data&quot;]/ul[@class=&quot;shipping-info&quot;]/li[@class=&quot;name&quot;]</value>
      <webElementGuid>cc6ee845-e6b6-4097-8c61-aa13985876c8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul[2]/li[2]</value>
      <webElementGuid>78650b5c-dca1-43a0-9709-fa174751c84b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Shipping Address'])[1]/following::li[1]</value>
      <webElementGuid>6eb626c3-f7d3-4504-8f3e-de89a060ceac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cash On Delivery (COD)'])[1]/following::li[2]</value>
      <webElementGuid>f6737a6c-1e56-4a34-8547-e0cbf9188f8b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email: testing19@mail.com'])[2]/preceding::li[1]</value>
      <webElementGuid>69bb097a-effc-4554-b05d-2e7b0cfe554e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Phone: 12937'])[2]/preceding::li[2]</value>
      <webElementGuid>f0f784cc-c596-4aad-9133-7236738b1aa8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/ul[2]/li[2]</value>
      <webElementGuid>7b9dd62b-f729-46d8-8724-e6e5b7e35e75</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
                        Jhon Doe
                    ' or . = '
                        Jhon Doe
                    ')]</value>
      <webElementGuid>6573d16b-70a5-4358-b035-d98fbcc53d76</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
