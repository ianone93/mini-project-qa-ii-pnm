<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_BillingNewAddress.PhoneNumber</name>
   <tag></tag>
   <elementGuidId>00f1cf99-8c63-487c-a91e-33e0f6d01cb3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='BillingNewAddress_PhoneNumber']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#BillingNewAddress_PhoneNumber</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>e2958dbd-e24c-40f4-b071-d0377190a0d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-box single-line valid</value>
      <webElementGuid>46637da7-1aaa-4c2d-9f9a-643fc318709a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>591a97a5-433e-4ee4-bd38-309a5e308dd7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val-required</name>
      <type>Main</type>
      <value>Phone is required</value>
      <webElementGuid>a279965a-a157-4f20-8280-c0ee460f3ef8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>BillingNewAddress_PhoneNumber</value>
      <webElementGuid>0cf4003c-0df5-476d-8d16-1facc082c234</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>BillingNewAddress.PhoneNumber</value>
      <webElementGuid>b97561ef-7f1c-421f-a3e0-857d5e4380e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>3ce64e04-4d39-41c4-9c21-2667b2d738e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;BillingNewAddress_PhoneNumber&quot;)</value>
      <webElementGuid>fefb5dfe-a8e7-4201-8308-5cc72070e6f2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='BillingNewAddress_PhoneNumber']</value>
      <webElementGuid>4641b018-781a-4e9e-b96a-7fcc5b6042af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='billing-new-address-form']/div/div/div/div[11]/input</value>
      <webElementGuid>f033d57a-1133-496b-964a-ba821240c23c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[11]/input</value>
      <webElementGuid>50e6a81f-e1af-4377-a72b-fa86692b2066</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'BillingNewAddress_PhoneNumber' and @name = 'BillingNewAddress.PhoneNumber' and @type = 'text']</value>
      <webElementGuid>eb4bba17-7821-4e85-b7b8-411c632d31bb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
