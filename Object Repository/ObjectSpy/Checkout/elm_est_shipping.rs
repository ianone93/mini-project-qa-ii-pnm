<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>elm_est_shipping</name>
   <tag></tag>
   <elementGuidId>ee8ad1d0-3625-49c1-9e74-fd281399d8e2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Zip / postal code:'])[1]/following::li[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>li.shipping-option-item</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>656e4088-b20c-4053-b8fa-2223f9fba2a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>shipping-option-item</value>
      <webElementGuid>42153232-fbf4-4a4d-9dbb-fbaf460d3b31</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                                Ground (0.00)
                            
                            
                                Compared to other shipping methods, like by flight or over seas, ground shipping is carried out closer to the earth
                            
                        </value>
      <webElementGuid>2463e6ba-82c6-4294-bc07-419cd865168e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page shopping-cart-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;order-summary-content&quot;]/form[1]/div[@class=&quot;cart-footer&quot;]/div[@class=&quot;cart-collaterals&quot;]/div[@class=&quot;shipping&quot;]/div[@class=&quot;estimate-shipping&quot;]/ul[@class=&quot;shipping-results&quot;]/li[@class=&quot;shipping-option-item&quot;]</value>
      <webElementGuid>6b52af5c-a885-4f0d-a197-76d86d4418cc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Zip / postal code:'])[1]/following::li[1]</value>
      <webElementGuid>c0341a31-b559-40ea-8623-bce58cac3240</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[1]/following::li[1]</value>
      <webElementGuid>e70c70c3-185b-4cca-9d17-9d1a7949d506</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next Day Air (0.00)'])[1]/preceding::li[1]</value>
      <webElementGuid>50ae969b-500f-4a19-8291-f2e3f91480b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div/ul/li</value>
      <webElementGuid>6b2faaa8-b556-448e-ba51-025138359539</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
                            
                                Ground (0.00)
                            
                            
                                Compared to other shipping methods, like by flight or over seas, ground shipping is carried out closer to the earth
                            
                        ' or . = '
                            
                                Ground (0.00)
                            
                            
                                Compared to other shipping methods, like by flight or over seas, ground shipping is carried out closer to the earth
                            
                        ')]</value>
      <webElementGuid>41d566b5-3f54-4dcf-ad9d-3395b897c457</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
