<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Email testing19mail.com</name>
   <tag></tag>
   <elementGuidId>f0eeea8e-09a6-44a9-8963-49e11e3786a4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul[2]/li[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ul.shipping-info > li.email</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>cdd40834-a3bb-4e24-ab08-1261db968f45</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>90599964-4f3a-472e-876e-f9c8681101ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Email: testing19@mail.com
                    </value>
      <webElementGuid>e1fe27dc-79d0-46ad-bde0-0817eb963fc0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-confirm-order-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;order-summary-body&quot;]/div[@class=&quot;order-summary-content&quot;]/div[@class=&quot;order-review-data&quot;]/ul[@class=&quot;shipping-info&quot;]/li[@class=&quot;email&quot;]</value>
      <webElementGuid>34cc4bb2-f39e-4eed-a24c-59d80794084c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul[2]/li[3]</value>
      <webElementGuid>252c3612-2ce6-4fa9-b81e-a70de30f44c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jhon Doe'])[2]/following::li[1]</value>
      <webElementGuid>33176016-ab8a-4d0c-9015-2097db1a4f6c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Shipping Address'])[1]/following::li[2]</value>
      <webElementGuid>c15d116c-bbdf-4b12-9b67-652324e59d5e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Phone: 12937'])[2]/preceding::li[1]</value>
      <webElementGuid>68eeb965-b3c0-49aa-9505-396c4a56bff5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fax:'])[2]/preceding::li[2]</value>
      <webElementGuid>1eddcda1-f133-43df-9cb7-de475946bb40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/ul[2]/li[3]</value>
      <webElementGuid>371b021a-173f-45a3-bd13-470dcba8cbef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
                        Email: testing19@mail.com
                    ' or . = '
                        Email: testing19@mail.com
                    ')]</value>
      <webElementGuid>a30a7894-705f-43cb-af0c-32843069c173</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
