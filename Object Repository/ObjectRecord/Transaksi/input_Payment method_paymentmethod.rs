<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Payment method_paymentmethod</name>
   <tag></tag>
   <elementGuidId>fd37e1de-7df7-4a01-a5af-124dcc8a096e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='paymentmethod_0']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#paymentmethod_0</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>a311f39f-9f13-452e-a203-ed96c5ee85c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>paymentmethod_0</value>
      <webElementGuid>a10856a4-d5bc-4ec9-8137-c667c2029ef0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>3749d634-fa3b-4588-83b3-73fae0e0d83b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>paymentmethod</value>
      <webElementGuid>374af1fc-ecb4-480d-b9f6-0d17a3ddc990</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Payments.CashOnDelivery</value>
      <webElementGuid>d45028f2-0892-45d7-b763-7911fe7e97aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>checked</name>
      <type>Main</type>
      <value>checked</value>
      <webElementGuid>59a30389-3fc3-469d-9bd4-3f6983193acb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;paymentmethod_0&quot;)</value>
      <webElementGuid>8ca6324f-cce9-479d-b2b2-475c0be87f08</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='paymentmethod_0']</value>
      <webElementGuid>bfc6590f-8bde-4cb1-81b7-f2bbfb84d1a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-method-load']/div/div/ul/li/div/div[2]/input</value>
      <webElementGuid>e7952e1d-0e03-47a1-81f3-82a37ca8639e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/div/div[2]/input</value>
      <webElementGuid>9e1e6e18-e2ef-4b74-8c53-713f257f84af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'paymentmethod_0' and @type = 'radio' and @name = 'paymentmethod' and @checked = 'checked']</value>
      <webElementGuid>11de6411-e98f-4473-b86a-c37b3eba0c58</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
