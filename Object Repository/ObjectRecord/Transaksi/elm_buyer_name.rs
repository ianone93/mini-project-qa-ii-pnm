<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>elm_buyer_name</name>
   <tag></tag>
   <elementGuidId>8aea2a07-68d7-49a3-826f-6213280c6b0d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul[2]/li[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ul.shipping-info > li.name</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>0d151ef3-c13c-47a2-b77d-2f5c86c85449</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>name</value>
      <webElementGuid>70f3fee1-325f-4d2c-833f-f0d8da8ecf62</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        doe jhon
                    </value>
      <webElementGuid>24d7808f-3cf0-42ac-bf71-a218bf4635af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-confirm-order-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;order-summary-body&quot;]/div[@class=&quot;order-summary-content&quot;]/div[@class=&quot;order-review-data&quot;]/ul[@class=&quot;shipping-info&quot;]/li[@class=&quot;name&quot;]</value>
      <webElementGuid>afce80d3-a760-467d-b4a5-4c432396e78f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul[2]/li[2]</value>
      <webElementGuid>5146f4ac-ba76-43b7-ae61-8835603f9fd5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Shipping Address'])[1]/following::li[1]</value>
      <webElementGuid>f2e190db-0eab-4721-92f0-3120dac77815</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cash On Delivery (COD)'])[1]/following::li[2]</value>
      <webElementGuid>aa73aa66-e9ce-44f2-b67b-7eb27fe4dc2f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email: test12762@mail.com'])[2]/preceding::li[1]</value>
      <webElementGuid>4abffebd-bbb5-4bcc-b94e-aceea9dd841e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Phone: 1234567'])[2]/preceding::li[2]</value>
      <webElementGuid>5491aa47-ecae-4064-8575-5ddc005b4475</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/ul[2]/li[2]</value>
      <webElementGuid>c994400d-f003-4b3e-8881-e977ada5f634</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
                        doe jhon
                    ' or . = '
                        doe jhon
                    ')]</value>
      <webElementGuid>d1085cf1-6b4e-4237-b0fe-0ae0e5890565</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
