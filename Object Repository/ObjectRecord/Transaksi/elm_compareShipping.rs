<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>elm_compareShipping</name>
   <tag></tag>
   <elementGuidId>04c71db6-0870-4907-9131-bf30e5b72c79</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Ground (0.00)'])[1]/following::span[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.option-description</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>eaa32920-ec45-4b82-9810-9c5501bc3354</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>option-description</value>
      <webElementGuid>ad20e127-52b5-40e7-9c71-e5fa82fe2bdb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                Compared to other shipping methods, like by flight or over seas, ground shipping is carried out closer to the earth
                            </value>
      <webElementGuid>4beef2a2-6de5-41f4-a4dd-172c61c46c8d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page shopping-cart-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;order-summary-content&quot;]/form[1]/div[@class=&quot;cart-footer&quot;]/div[@class=&quot;cart-collaterals&quot;]/div[@class=&quot;shipping&quot;]/div[@class=&quot;estimate-shipping&quot;]/ul[@class=&quot;shipping-results&quot;]/li[@class=&quot;shipping-option-item&quot;]/span[@class=&quot;option-description&quot;]</value>
      <webElementGuid>cf6a6ace-f251-4da9-9fee-f93ecb527d1d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ground (0.00)'])[1]/following::span[1]</value>
      <webElementGuid>80f607a1-ae3e-4239-a352-9572d2f7a482</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Zip / postal code:'])[1]/following::span[1]</value>
      <webElementGuid>2aa36a16-172d-4992-8c52-5c54b5d9ef60</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next Day Air (0.00)'])[1]/preceding::span[1]</value>
      <webElementGuid>a074a27d-07f1-4934-9eff-0f312f36bff0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The one day air shipping'])[1]/preceding::span[1]</value>
      <webElementGuid>eac978fd-0ccb-44a7-94e7-7fb250f2ae2d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Compared to other shipping methods, like by flight or over seas, ground shipping is carried out closer to the earth']/parent::*</value>
      <webElementGuid>df8356f3-3af3-424b-b1d4-034301535a67</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/ul/li/span</value>
      <webElementGuid>708f255a-0c1e-47ca-80ad-76f9386a6f62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '
                                Compared to other shipping methods, like by flight or over seas, ground shipping is carried out closer to the earth
                            ' or . = '
                                Compared to other shipping methods, like by flight or over seas, ground shipping is carried out closer to the earth
                            ')]</value>
      <webElementGuid>c290625a-8361-4305-a04b-9f2752e495bb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
