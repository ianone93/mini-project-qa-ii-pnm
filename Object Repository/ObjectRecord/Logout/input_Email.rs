<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Email</name>
   <tag></tag>
   <elementGuidId>7e6f5b74-4945-4a00-99a8-644e5ad17b3b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#Email</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='Email']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>b5528f43-4169-468c-b0ed-4cd823b32c4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autofocus</name>
      <type>Main</type>
      <value>autofocus</value>
      <webElementGuid>0354254b-d373-4263-b10f-ace4f9093903</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>5d695d2f-94bc-4932-8ebe-5841fd66a78a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Email</value>
      <webElementGuid>eeae7f64-f2dd-44dd-9469-37a98a02e327</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Email</value>
      <webElementGuid>7b4ec78c-744c-4f98-8d77-f1068246f7e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>818cbec5-d068-43bb-ab1b-0a62f18f7c7e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Email&quot;)</value>
      <webElementGuid>31fdc83c-6eef-46ef-8d71-6d11673308a0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='Email']</value>
      <webElementGuid>733f0a78-85b9-47a7-ac45-aed9d1d10dd9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div[2]/input</value>
      <webElementGuid>0583dc76-47ce-48f2-a936-660abf5cbe55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'Email' and @name = 'Email' and @type = 'text']</value>
      <webElementGuid>24752749-72a6-4c3e-ab4a-1c0478b5ca73</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
