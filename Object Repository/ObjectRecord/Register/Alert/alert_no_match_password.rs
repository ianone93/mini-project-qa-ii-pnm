<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>alert_no_match_password</name>
   <tag></tag>
   <elementGuidId>177a2cad-c09a-4253-8fb4-ab9666330c54</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[(text() = 'The password and confirmation password do not match.' or . = 'The password and confirmation password do not match.')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>5d2d8b3b-1e0a-491f-8c44-034be997ef04</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>ConfirmPassword</value>
      <webElementGuid>33580a30-379b-42e4-b8ad-a87a952efb63</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>The password and confirmation password do not match.</value>
      <webElementGuid>a3139bbc-25ed-4aff-8ebc-f6d6cf327d67</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/span[1]</value>
      <webElementGuid>aa1f5a0d-ed8f-4c80-a4f1-a9ba3e04bc2e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'The password and confirmation password do not match.' or . = 'The password and confirmation password do not match.')]</value>
      <webElementGuid>a2569a9d-5c8a-4621-a8b5-91a3a5df2e9e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
