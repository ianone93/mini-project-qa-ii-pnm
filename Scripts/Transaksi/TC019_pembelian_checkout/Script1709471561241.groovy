import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Keranjang/TC017_add_item_to_cart'), [('email') : email, ('password') : password], FailureHandling.STOP_ON_FAILURE)

WebUI.selectOptionByValue(findTestObject('Object Repository/ObjectRecord/Transaksi/select_country'), '42', true)

WebUI.setText(findTestObject('Object Repository/ObjectRecord/Transaksi/input_zipcode_ZipPostalCode'), '42384')

WebUI.click(findTestObject('Object Repository/ObjectRecord/Transaksi/btn_zipcode_estimateshipping'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ObjectRecord/Transaksi/elm_compareShipping'))

WebUI.click(findTestObject('Object Repository/ObjectRecord/Transaksi/chkbox_terms'))

WebUI.click(findTestObject('Object Repository/ObjectRecord/Transaksi/button_Checkout'))

WebUI.selectOptionByValue(findTestObject('Object Repository/ObjectRecord/Transaksi/select_country_2'), '42', true)

WebUI.setText(findTestObject('Object Repository/ObjectRecord/Transaksi/input_City_BillingNewAddress.City'), 'jaksel')

WebUI.setText(findTestObject('Object Repository/ObjectRecord/Transaksi/input_BillingNewAddress.Address1'), 'okay')

WebUI.setText(findTestObject('Object Repository/ObjectRecord/Transaksi/input_BillingNewAddress.Address2'), '')

WebUI.click(findTestObject('Object Repository/ObjectRecord/Transaksi/input_BillingNewAddress.Address2'))

WebUI.setText(findTestObject('Object Repository/ObjectRecord/Transaksi/input_zipcode_BillingNewAddress'), '72837')

WebUI.setText(findTestObject('Object Repository/ObjectRecord/Transaksi/input_BillingNewAddress.PhoneNumber'), '1234567')

WebUI.click(findTestObject('ObjectRecord/Transaksi/button/btn_billing'))

WebUI.delay(3)

WebUI.click(findTestObject('ObjectRecord/Transaksi/button/btn_shippingAddress'))

WebUI.delay(2)

WebUI.click(findTestObject('ObjectRecord/Transaksi/button/btn_shippingMethod'))

WebUI.delay(2)

WebUI.click(findTestObject('ObjectRecord/Transaksi/button/btn_paymentMethod'))

WebUI.delay(2)

WebUI.click(findTestObject('ObjectRecord/Transaksi/button/btn_paymentInfo'))

WebUI.delay(2)

WebUI.click(findTestObject('ObjectRecord/Transaksi/button/btn_confirm'))

WebUI.waitForPageLoad(0)

WebUI.verifyElementText(findTestObject('Object Repository/ObjectRecord/Transaksi/alert_order_success'), 'Your order has been successfully processed!')

WebUI.closeBrowser()

