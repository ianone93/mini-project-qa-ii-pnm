import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Login/positive/TC009_normal_login'), [('email') : email, ('password') : password], FailureHandling.STOP_ON_FAILURE)

WebUI.waitForPageLoad(0)

WebUI.setText(findTestObject('ObjectSpy/Search/input_search'), '14.1-inch Laptop')

WebUI.delay(3)

WebUI.click(findTestObject('ObjectSpy/Search/btn_search'))

WebUI.waitForPageLoad(0)

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Search/results_thumbnail'))

WebUI.click(findTestObject('ObjectSpy/Cart/button/btn_addtoCart'))

WebUI.waitForPageLoad(0)

WebUI.verifyElementText(findTestObject('ObjectRecord/Cart/alert_item_added'), 'The product has been added to your shopping cart')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ObjectRecord/Cart/link_cart'))

WebUI.waitForPageLoad(0)

WebUI.verifyElementVisible(findTestObject('ObjectRecord/Cart/cart_item_name'), FailureHandling.STOP_ON_FAILURE)

